export default {
  install: (Vue) => {
    const storage = {
      get (keyName) {
        const value = localStorage.getItem(keyName)
        if (value) {
          return JSON.parse(value)
        }
        return null
      },
      set (keyName, datos) {
        const value = JSON.stringify(datos || {})
        localStorage.setItem(keyName, value)
      },
      clear () {
        localStorage.clear()
      }
    }
    Vue.prototype.$storage = storage;
  }
};